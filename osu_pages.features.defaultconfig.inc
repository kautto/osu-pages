<?php
/**
 * @file
 * osu_pages.features.defaultconfig.inc
 */

/**
 * Implements hook_defaultconfig_features().
 */
function osu_pages_defaultconfig_features() {
  return array(
    'osu_pages' => array(
      'ds_field_settings_info' => 'ds_field_settings_info',
    ),
  );
}

/**
 * Implements hook_defaultconfig_ds_field_settings_info().
 */
function osu_pages_defaultconfig_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|osu_page|featured';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'osu_page';
  $ds_fieldsetting->view_mode = 'featured';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'node_link' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link text' => 'LEARN MORE',
        'wrapper' => '',
        'class' => '',
        'ft' => array(),
      ),
    ),
    'osu_body_plain_trimmed' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'field_osu_image_4x3' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_slideshow_image',
        ),
      ),
    ),
  );
  $export['node|osu_page|featured'] = $ds_fieldsetting;

  return $export;
}
