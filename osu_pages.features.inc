<?php
/**
 * @file
 * osu_pages.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function osu_pages_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function osu_pages_node_info() {
  $items = array(
    'osu_page' => array(
      'name' => t('Page'),
      'base' => 'node_content',
      'description' => t('A page with an optional sidebar.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
