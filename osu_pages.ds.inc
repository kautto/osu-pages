<?php
/**
 * @file
 * osu_pages.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function osu_pages_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|osu_page|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'osu_page';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'active_page_submenu' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h1',
        'class' => '',
        'ft' => array(),
      ),
    ),
    'field_osu_sidebars' => array(
      'formatter_settings' => array(
        'ft' => array(
          'func' => 'theme_ds_field_expert',
          'ow' => TRUE,
          'ow-el' => 'div',
          'ow-cl' => 'page-sidebar',
          'ow-def-cl' => FALSE,
          'ow-at' => '',
          'ow-def-at' => FALSE,
          'fi' => TRUE,
          'fi-el' => 'div',
          'fi-cl' => 'page-sidebar-item',
          'fi-at' => '',
          'fi-def-at' => FALSE,
          'fi-odd-even' => TRUE,
        ),
      ),
    ),
  );
  $export['node|osu_page|default'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function osu_pages_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|osu_page|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'osu_page';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'panels-osu_2col_flex';
  $ds_layout->settings = array(
    'regions' => array(
      'content' => array(
        0 => 'field_osu_body',
      ),
      'sidebar_2' => array(
        1 => 'field_osu_sidebars',
      ),
    ),
    'fields' => array(
      'field_osu_body' => 'content',
      'field_osu_sidebars' => 'sidebar',
    ),
    'classes' => array(),
    'wrappers' => array(
      'content' => 'div',
      'sidebar_2' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|osu_page|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|osu_page|featured';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'osu_page';
  $ds_layout->view_mode = 'featured';
  $ds_layout->layout = 'ds_2col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_osu_image_4x3',
      ),
      'right' => array(
        1 => 'title',
        2 => 'osu_body_plain_trimmed',
        3 => 'node_link',
      ),
    ),
    'fields' => array(
      'field_osu_image_4x3' => 'left',
      'title' => 'right',
      'osu_body_plain_trimmed' => 'right',
      'node_link' => 'right',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|osu_page|featured'] = $ds_layout;

  return $export;
}
